#include "tostring.hpp"
#include <thero/assert.hpp>
#include <sstream>

namespace spr
{
std::vector<std::string> toStringList(AxisOrientation orientation)
{
    std::string text(AxisOrientation_value_to_string(orientation));
    return
     {
      text,
     };
}std::vector<std::string> toStringList(HOrientation orientation)
{
    std::string text(HOrientation_value_to_string(orientation));
    return
     {
      text,
     };
}
}
