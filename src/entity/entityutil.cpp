#include "entityutil.hpp"
#include <dpx/insert.hpp>
#include <dpx/erase.hpp>
#include <dpx/has.hpp>
#include <dpx/get.hpp>
#include <dpx/find.hpp>

#include <spr/data/position.hpp>
#include <spr/data/hitbox.hpp>
#include <spr/data/entitycollider.hpp>
#include <spr/data/sceneparent.hpp>
#include <spr/entity/entityutil.hpp>
#include <constants/world.hpp>
#include <gamedata.hpp>
#include <entityaddremove.hpp>
#include <data/camerafollow.hpp>

dpx::TableId addEntity(spr::EntityProperties properties, Scene& scene, GameData& data)
{
    dpx::TableId newId = spr::addEntity(properties, data.sprg, scene.spr);

    auto entityCollider = properties.find("entity_collider"_hash);
    if(entityCollider != properties.end())
    {
        glm::vec3 position = std::any_cast<spr::Position>(properties.at("position"_hash)).coordinate;

        auto hitboxIter = properties.find("hitbox"_hash);
        TH_ASSERT(hitboxIter != properties.end(), "entity_collider must have hitbox");
        const spr::Hitbox& hitbox = std::any_cast<const spr::Hitbox&>(hitboxIter->second);

        data.spatialEntityStorage.insert(newId, {position.x, position.y}, {hitbox.aabb.size.x, hitbox.aabb.size.y});
    }

    //game tables
    addEntityGenerated(newId, properties, scene.game);

    return newId;
}

void removeEntity(dpx::TableId entityId, Scene& scene, GameData& data)
{
    TH_ASSERT(has(entityId, *scene.spr.tSceneParent) , "Removing nonexisting entity " << entityId);

    //remove any camera followings connected to this entity
    eraseIf([entityId](dpx::TableId, const CameraFollow& follow)
    {
        return follow.entityId == entityId;
    }, *data.worldScene->game.tCameraFollow);

    if(data.entitiesToRemove.count(entityId) == 0)
    {
        data.entitiesToRemove.insert(entityId);
    }
}

void removeEntityData(dpx::TableId entityId, Scene& scene, GameData& data)
{
    if(has(entityId, *scene.spr.tEntityCollider))
        data.spatialEntityStorage.erase(entityId);

    spr::removeEntityData(entityId, data.sprg, scene.spr);

    removeEntityDataGenerated(entityId, scene.game);
}

void clearAllCurrentEntities(Scene& scene, GameData& data)
{
    forEach([&] (dpx::TableId id, const spr::Position& position)
    {
        removeEntity(id, scene, data);
    }, *scene.spr.tPosition);
}

bool withinSpatialBounds(glm::vec2 position, GameData& data)
{
    if(!withinSpatialBoundsX(position.x, data) || !withinSpatialBoundsY(position.y, data))
        return false;
    else
        return true;
}

bool withinSpatialBoundsX(float x, GameData& data)
{
    float halfSpatialStorageSize = data.c->world->spatialStorageSize / 2.0f;

    if(x < -halfSpatialStorageSize + 1.0f || x > halfSpatialStorageSize - 1.0f)
        return false;
    else
        return true;
}

bool withinSpatialBoundsY(float y, GameData& data)
{
    float halfSpatialStorageSize = data.c->world->spatialStorageSize / 2.0f;

    if(y < -halfSpatialStorageSize + 1.0f || y > halfSpatialStorageSize - 1.0f)
        return false;
    else
        return true;
}
