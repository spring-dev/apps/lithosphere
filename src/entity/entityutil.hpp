#pragma once
#include <cstdint>
#include <spr/entity/entityproperties.hpp>

struct GameData;
struct Scene;

dpx::TableId addEntity(spr::EntityProperties properties, Scene& scene, GameData& data);
void removeEntity(dpx::TableId entityId, Scene& scene, GameData& data);
void removeEntityData(dpx::TableId entityId, Scene& scene, GameData& data);
void clearAllCurrentEntities(Scene& scene, GameData& data);
bool withinSpatialBounds(glm::vec2 position, GameData& data);
bool withinSpatialBoundsX(float x, GameData& data);
bool withinSpatialBoundsY(float y, GameData& data);
