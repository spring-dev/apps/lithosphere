#include "entitylogic.hpp"
#include <dpx/join.hpp>
#include "entityutil.hpp"
#include <gamedata.hpp>
#include <tracy.hpp>

void removeDeletedEntities(GameData& data)
{
    for(dpx::TableId id : data.entitiesToRemove)
    {
        removeEntityData(id, *data.worldScene, data);
    }

    data.entitiesToRemove.clear();
}

void updateSpatialTree(GameData& data)
{
    ZoneScopedN("update_spatial_tree");
    join([&] (dpx::TableId id, const spr::EntityCollider&, const spr::WorldPosition& position)
    {
        data.spatialEntityStorage.move(id, {position.coordinate.x, position.coordinate.y});
    }, *data.worldScene->spr.tEntityCollider, *data.worldScene->spr.tWorldPosition);
}
