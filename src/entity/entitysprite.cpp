#include "entitysprite.hpp"
#include <dpx/join.hpp>
#include <spr/data/sprite.hpp>
#include <spr/data/animatedsprite.hpp>
#include <data/horizontalorientation.hpp>
#include <gamedata.hpp>

void updateEntitySprites(struct GameData& data)
{
    forEach([&](dpx::TableId spriteId, spr::Sprite& sprite)
    {
        auto animatedSprite = findId(spriteId, *data.worldScene->spr.tAnimatedSprite);
        if(animatedSprite)
        {
            ++animatedSprite->animationClock;
        }
    }, *data.worldScene->spr.tSprite);

    dpx::join([&](dpx::TableId id, spr::Sprite& sprite, const HorizontalOrientation& orientation)
    {
        sprite.horizontalFlip = orientation.orientation == HOrientation::Left;
    }, *data.worldScene->spr.tSprite, *data.worldScene->game.tHorizontalOrientation);
}