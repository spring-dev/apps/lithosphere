#pragma once
#include <optional>
#include <dpx/tableid.hpp>

void updateCameraFollow(struct GameData& data);
void setCameraFollow(dpx::TableId camId, std::optional<dpx::TableId> entityId, struct GameData& data);
