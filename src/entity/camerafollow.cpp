#include "camerafollow.hpp"
#include <spr/data/worldposition.hpp>
#include <spr/data/camera.hpp>
#include <data/camerafollow.hpp>
#include <gamedata.hpp>

constexpr float cCameraDistance = 100.0f;

void updateCameraFollow(struct GameData& data)
{
    forEach([&](dpx::TableId, const CameraFollow& follow)
    {
        glm::vec2 position = get(follow.entityId, *data.worldScene->spr.tWorldPosition).coordinate;

        spr::Camera& cam = get(follow.cameraId, *data.sprg.tCamera);

        cam.translation = {position.x, position.y, cCameraDistance};
    }, *data.worldScene->game.tCameraFollow);
}

void setCameraFollow(dpx::TableId camId, std::optional<dpx::TableId> entityId, GameData& data)
{
    eraseIf([camId](dpx::TableId, const CameraFollow& follow)
    {
        return follow.cameraId == camId;
    }, *data.worldScene->game.tCameraFollow);

    if(entityId)
    {
        insert({.cameraId=camId, .entityId=*entityId}, *data.worldScene->game.tCameraFollow);
    }
}
