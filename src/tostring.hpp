#pragma once
#include <string>
#include <vector>
#include <behaviors/clonkdefines.hpp>
#include <common/orientation.hpp>

namespace spr
{
std::vector<std::string> toStringList(AxisOrientation orientation);
std::vector<std::string> toStringList(HOrientation orientation);
}
