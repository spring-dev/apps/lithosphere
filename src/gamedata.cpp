#include "gamedata.hpp"
#include <data/alltypes.hpp>
#include <spr/data/alltypes.hpp>

GameData::GameData()
{
    worldScene = std::make_unique<Scene>();
    uiScene = std::make_unique<Scene>();

    instantiateTables(sprg);
    instantiateTables(gamg);
    instantiateTables(worldScene->spr);
    instantiateTables(uiScene->spr);
    instantiateTables(worldScene->game);
    instantiateTables(uiScene->game);
}

GameData::~GameData()
{
}
