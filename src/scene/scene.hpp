#pragma once
#include <spr/data/tables.hpp>
#include <data/tables.hpp>

struct Scene
{
    spr::SceneTables spr;
    gam::SceneTables game;
};
