#include "orientation.hpp"
#include <data/horizontalorientation.hpp>
#include <gamedata.hpp>

void setOrientation(dpx::TableId id, HOrientation orientation, GameData& data)
{
    if(has(id, *data.worldScene->game.tHorizontalOrientation))
        set(id, HorizontalOrientation{orientation}, *data.worldScene->game.tHorizontalOrientation);
}