#pragma once
#include <thero/meta_enum.hpp>
#include <dpx/tableid.hpp>

meta_enum_class(AxisOrientation, int8_t, Horizontal, Vertical);
meta_enum_class(HOrientation, int8_t, Left, Right);

void setOrientation(dpx::TableId id, HOrientation orientation, struct GameData& data);