#pragma once
#include <spr/hash/stringhash.hpp>

namespace GlobalAction
{
    constexpr spr::StringHash Pause = "global_pause"_hash;
    constexpr spr::StringHash AdvanceFrame = "global_advance_frame"_hash;
}
