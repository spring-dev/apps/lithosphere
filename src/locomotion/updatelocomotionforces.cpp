#include "updatelocomotionforces.hpp"
#include <locomotion/groundlocomotor.hpp>

void updateLocomotionForces(struct GameData &data)
{
    updateGroundLocomotors(data);
}
