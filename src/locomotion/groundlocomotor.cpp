#include "groundlocomotor.hpp"
#include <gamedata.hpp>
#include <dpx/join.hpp>
#include <spr/physics/accelerate.hpp>
#include <spr/data/physics.hpp>
#include <data/groundlocomotor.hpp>
#include <data/moveintention.hpp>

void updateGroundLocomotors(GameData& data)
{
    dpx::join([&](dpx::TableId id, spr::Physics& physics, GroundLocomotor& locomotor)
    {
        const MoveIntention* moveIntention = findId(id, *data.worldScene->game.tMoveIntention);
        glm::vec2 direction = moveIntention ? moveIntention->direction : glm::vec2{};

        float maxVelocity = locomotor.maxVelocity;
        float maxAcceleration = locomotor.maxAcceleration;

        float height = 0.0f;

        //things in the air cannot move
        if(height == 0.0f)
        {
            glm::vec2 currentVelocity = physics.velocity;
            glm::vec2 acceleration = spr::accelerate(direction, maxVelocity, currentVelocity, maxAcceleration);

            physics.acceleration = acceleration;
        }
        else
        {
            physics.acceleration = {};
        }
    }, *data.worldScene->spr.tPhysics, *data.worldScene->game.tGroundLocomotor);
}
