#pragma once

namespace DebugCode
{
constexpr int game = 0;
constexpr int render = 1;
}

void updateDebugDrawables(struct Scene &scene);