#include "debugrendering.hpp"
#include <scene/scene.hpp>
#include <dpx/join.hpp>
#include <spr/data/worldposition.hpp>
#include <spr/data/physics.hpp>
#include <data/physicalvertices.hpp>
#include <spr/debug/debugrenderer.hpp>
#include <physics/collidecode.hpp>

void updateDebugDrawables(Scene &scene)
{
    dpx::join([](dpx::TableId id, spr::Physics& physics)
    {
        physics.velocity += physics.acceleration;
        physics.acceleration = {};
    }, *scene.spr.tPhysics);

    dpx::join([](dpx::TableId id, const spr::WorldPosition& position, const PhysicalVertices& vertices)
    {
        for(size_t i = 0; i < vertices.coordinates.size(); ++i)
        {

            glm::vec2 worldPos = spr::swizzle("xy", position.coordinate) + vertices.coordinates[i];

            spr::Color col;

            switch(vertices.codes[i])
            {
                case CollideCode::None:
                    col = spr::Color::Red();
                    break;
                case CollideCode::Top:
                    col = spr::Color::Cyan();
                    break;
                case CollideCode::Bottom:
                    col = spr::Color::Green();
                    break;
                case CollideCode::Left:
                    col = spr::Color::Yellow();
                    break;
                case CollideCode::Right:
                    col = spr::Color::Orange();
                    break;
                default:
                    col = spr::Color::Pink();
                    break;
            }
            spr::DRen::out << spr::DRen::make(spr::DebugStar{.position=worldPos, .radius=0.5f}, col);
        }
    }, *scene.spr.tWorldPosition, *scene.game.tPhysicalVertices);
}