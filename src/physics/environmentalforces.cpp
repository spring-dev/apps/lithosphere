#include "environmentalforces.hpp"
#include <data/environmentalforces.hpp>
#include <scene/scene.hpp>
#include <dpx/join.hpp>
#include <spr/data/physics.hpp>

void updateEnvironmentalForces(Scene& scene)
{
    dpx::join([](dpx::TableId id, const EnvironmentalForces& forces, spr::Physics& physics)
    {
        physics.acceleration += forces.gravityForce;
    }, *scene.game.tEnvironmentalForces, *scene.spr.tPhysics);
}
