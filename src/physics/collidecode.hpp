#pragma once

namespace CollideCode
{
    constexpr int None = 0;
    constexpr int Left = 1;
    constexpr int Right = 2;
    constexpr int Top = 3;
    constexpr int Bottom = 4;
}