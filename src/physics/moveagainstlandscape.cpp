#include "moveagainstlandscape.hpp"
#include <gamedata.hpp>
#include <spr/data/position.hpp>
#include <spr/data/physics.hpp>
#include <data/physicalvertices.hpp>
#include <data/landscapecollider.hpp>

constexpr int cGridSize = 1; //size of the single units
constexpr float cRestitution = 0.0f;

std::vector<LandscapeCollision> moveAgainstLandscape(GameData& data)
{
    std::vector<LandscapeCollision> result;

    dpx::join([&](dpx::TableId id, spr::Position& entityPositionIn, spr::Physics& physics, const PhysicalVertices& vertices, const LandscapeCollider& landscapeCollider)//, const spr::Hitbox& hitbox)
    {
        WorldCoordinate entityPosition{spr::swizzle("xy", entityPositionIn.coordinate)};
        WorldCoordinate entityVelocity{physics.velocity};

        //const spr::AABB& collisionBox = hitbox.aabb;

        //if we are not moving, we don't need to do anything
        if(glm::length(entityVelocity.glm()) == 0.0f)
            return dpx::LoopResult::Continue;

        float totalDistanceToTravel;
        float distanceTravelled;
        std::optional<LandscapeCollision> collision; //stores potential collision
        do
        {
            //total distance that the entity wants to move based on velocity
            //is modified when collision happens
            totalDistanceToTravel = glm::length(entityVelocity.glm());

            distanceTravelled = 0.0f; //??? really 0 here?

            //where the entity wants to be according to current velocity
            WorldCoordinate targetPos = entityPosition + entityVelocity;

            //the ray that traverses the pixel landscape starts here. it uses an arbitrary vertex atm. we don't wanna
            //ray every single vertex, we just need to ray one, and then check the others for collisions along the way.
            WorldCoordinate rayStartPosition = spr::swizzle("xy", vertices.coordinates.front()) + entityPosition;
            WorldCoordinate rayEndPosition = spr::swizzle("xy", vertices.coordinates.front()) + targetPos;

            //convert the ray positions to the pixel cell positions (pixel == cell)
            //to check if the movement we want to do would even take us one pixel away
            bool onlyMovesWithinAPixel = convertTo<UnitCoordinate>(rayStartPosition) == convertTo<UnitCoordinate>(rayEndPosition);

            //do the ray traverse in the grid unless we are moving so slowly that we didn't even cross a single pixel boundary
            if(!onlyMovesWithinAPixel)
            {
                //the data we are colliding against
                const auto& collisionMap = data.lithosData.types;

                //now with all data prepared we can call the actual grid raycast function. this function will
                //cast the ray through the grid and run the given lambda for each cell. The given lambda checks
                //for collision and returns if it hit or not. If it did hit, then the ray traversing will be terminated.
                //any collision will be stored in the optional along with any relevant data

                stc::static_vector<glm::vec2, vertices.coordinates.capacity()> rayLocalCollisionPoints = vertices.coordinates;

                for(glm::vec2& collisionPoint : rayLocalCollisionPoints)
                {
                    collisionPoint -= vertices.coordinates.front();
                }

                gridRayTraverse(rayStartPosition, rayEndPosition, {cGridSize, cGridSize}, [&collisionMap, &rayLocalCollisionPoints, &collision] (UnitCoordinate cellCoordinate, UnitCoordinate delta, const glm::vec2& hitPosition, float distanceTravelled)
                {
                    LithosType cellMaterial = {};
                    bool hit = false;
                    glm::vec2 impactPoint;

                    //do the actual hit detection against the landscape
                    for(glm::vec2 rayLocalPoint : rayLocalCollisionPoints)
                    {
                        UnitCoordinate point = convertTo<UnitCoordinate>(WorldCoordinate{convertTo<WorldCoordinate>(cellCoordinate) + rayLocalPoint});
                        cellMaterial = collisionMap.atOr(point, LithosType::Air);
                        if(cellMaterial != LithosType::Air)
                        {
                            impactPoint = convertTo<WorldCoordinate>(point).glm();
                            hit = true;
                            break;
                        }
                    }

                    if(hit)
                    {
                        //we have a hit. calculate some collision data to store in the collision object

                        LandscapeCollision::CollisionType type;
                        if(distanceTravelled == 0.0f)
                            type = LandscapeCollision::Inside;
                        else if(delta.x != 0)
                            type = LandscapeCollision::Vertical;
                        else
                            type = LandscapeCollision::Horizontal;

                        //some of this data is filled out on the outside of this lambda
                        collision = LandscapeCollision
                        {
                            .colliderId={}, //filled in outside
                            .wallType=cellMaterial, //what did we collide with?
                            .impactPoint=impactPoint,
                            .impactVelocity={}, //filled in outside
                            .collisionType=type, //vertical or horizontal or inside collision
                            .travelledBefore=distanceTravelled, //how far did we travel before we collided?
                        };
                    }

                    return hit;
                });


                if(collision)
                {
                    //fill in the missing collision data
                    collision->colliderId = id;
                    collision->impactVelocity = entityVelocity;

                    //accumulate how far we have travelled
                    distanceTravelled += collision->travelledBefore;

                    if(!landscapeCollider.stopMovement || distanceTravelled >= totalDistanceToTravel)
                    {//done, nothing more to travel, so there is no collision ???????
                        entityPosition = targetPos;
                        distanceTravelled = totalDistanceToTravel;
                    }
                    else
                    {//handle collision
                        //move up to the collision point if we are meant to stop movement, otherwise don't
                        entityPosition += glm::normalize(entityVelocity) * collision->travelledBefore;

                        /*
                        if(collision->collisionType == LandscapeCollision::Inside)
                        {
                            //this collision means that we are actually stuck inside solid material
                            entityVelocity *= -cRestitution;
                        }
                        else if(collision->collisionType == LandscapeCollision::Vertical)
                        {
                            //vertical collision, reduce horizontal movement
                            entityVelocity.x *= -cRestitution;
                            //position.x += (collision->impactVelocity.x < 0.0f ? -1.0f : 1.0f);
                        }
                        else if (collision->collisionType == LandscapeCollision::Horizontal)
                        {
                            //horizontal collision, reduce vertical movement
                            entityVelocity.y *= -cRestitution;
                            //position.y += (collision->impactVelocity.y < 0.0f ? -1.0f : 1.0f);
                        }*/
                    }

                    //store the collision for other systems to use if they wish
                    result.push_back(*collision);
                }
                else
                {
                    //no collision, go ahead and move the current increment
                    entityPosition += glm::normalize(entityVelocity) * (totalDistanceToTravel - distanceTravelled);
                    distanceTravelled = totalDistanceToTravel;
                }

            }
            else
            {
                //we moved so slowly that we didn't cross a pixel boundary. that means we are free to move since we are only
                //moving within a single pixel
                entityPosition = WorldCoordinate(entityPosition + entityVelocity);
                distanceTravelled += glm::length(entityVelocity.glm());
            }
        } while(distanceTravelled < totalDistanceToTravel && !collision);

        //write the change in our entity data
        entityPositionIn.coordinate.x = entityPosition.x;
        entityPositionIn.coordinate.y = entityPosition.y;

        return dpx::LoopResult::Continue;
    }, *data.worldScene->spr.tPosition, *data.worldScene->spr.tPhysics, *data.worldScene->game.tPhysicalVertices, *data.worldScene->game.tLandscapeCollider); //*data.worldScene->spr.tHitbox);

    return result;
}