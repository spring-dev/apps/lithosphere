#include "acceleration.hpp"
#include <scene/scene.hpp>
#include <dpx/join.hpp>
#include <spr/data/physics.hpp>

void applyAndResetAcceleration(Scene& scene)
{
    dpx::join([](dpx::TableId id, spr::Physics& physics)
    {
        physics.velocity += physics.acceleration;
        physics.acceleration = {};
    }, *scene.spr.tPhysics);
}