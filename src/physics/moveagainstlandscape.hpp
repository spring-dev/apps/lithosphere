#pragma once
#include <vector>
#include <dpx/tableid.hpp>
#include <glm/glm.hpp>
#include "lithos/lithos.hpp"

struct LandscapeCollision
{
    enum CollisionType { Horizontal, Vertical, Inside };
    dpx::TableId colliderId;
    LithosType wallType;
    glm::vec2 impactPoint;
    glm::vec2 impactVelocity;
    CollisionType collisionType;
    float travelledBefore;
};

template <typename Functor>
void gridRayTraverse(const glm::vec2& origin, const glm::vec2& target, const glm::vec2& cellSize, Functor&& functor)
{
    //sweeping by using a ray traversal for the top left corner, then check the edges of the bounding box as it moves along
    //initialisation
    //NOTE: how to deal with 0.0f in velocity?
    //NOTE: still misses sometimes... maybe detect a miss and terminate
    glm::ivec2 startCell = origin / cellSize;
    glm::ivec2 stopCell = target / cellSize;

    glm::vec2 velocity = target - origin;

    //std::cout << "t: " << target << "\n";
    //std::cout << "o: " << origin << "\n";

    float lineCoefficient = velocity.x != 0.0f ? (velocity.y / velocity.x + 0.00000001f) : 1000000.0f;
    float lineConstant = origin.y - lineCoefficient * origin.x;

    //std::cout << "v: " << velocity << "\n";
    //std::cout << "l: " << lineCoefficient << "\n";
    //std::cout << "c: " << lineConstant << "\n";

    glm::ivec2 directionSigns
            {
                    velocity.x > 0.0f ? 1 : -1,
                    velocity.y > 0.0f ? 1 : -1,
            };

    glm::vec2 nextBoundaries = static_cast<glm::vec2>(startCell + directionSigns) * cellSize;

    if(directionSigns.x < 0)
        nextBoundaries.x += cellSize.x;
    if(directionSigns.y < 0)
        nextBoundaries.y += cellSize.y;


    //std::cout << "lineCoefficient: " << lineCoefficient << "\n";
    //std::cout << "lineConstant: " << lineConstant << "\n";
    glm::vec2 nextVerticalHit
            {
                    nextBoundaries.x,
                    nextBoundaries.x * lineCoefficient + lineConstant,
            };

    glm::vec2 nextHorizontalHit
            {
                    (nextBoundaries.y - lineConstant) / lineCoefficient,
                    nextBoundaries.y,
            };

    glm::vec2 distanceToNextHit
            {
                    glm::distance(origin, nextVerticalHit),
                    glm::distance(origin, nextHorizontalHit),
            };

    //std::cout << "o: " << origin << " nextHorizontalHit: " << nextHorizontalHit << " nextVerticalHit: " << nextVerticalHit << "\n";

    glm::vec2 rayDeltaBetweenBoundaries
            {
                    glm::length(glm::vec2(cellSize.x, cellSize.x * lineCoefficient)),
                    glm::length(glm::vec2(cellSize.y, cellSize.y / lineCoefficient)),
            };

    glm::ivec2 currentCell = startCell;

    auto isDone = [&](glm::ivec2 cell)
    {
        bool xSatisfied = directionSigns.x < 0 ? cell.x <= stopCell.x : cell.x >= stopCell.x;
        bool ySatisfied = directionSigns.y < 0 ? cell.y <= stopCell.y : cell.y >= stopCell.y;

        return xSatisfied && ySatisfied;
    };

    while(!isDone(currentCell))
    {
        //std::cout << "dd: " << distanceToNextHit << "\n";
        if(distanceToNextHit.x < distanceToNextHit.y)
        {
            distanceToNextHit.x = distanceToNextHit.x + rayDeltaBetweenBoundaries.x;
            currentCell.x += directionSigns.x;

            float xHit = currentCell.x * cellSize.x + (directionSigns.x < 0.0f ? cellSize.x : 0.0f);
            float yHit = xHit * lineCoefficient + lineConstant;

            glm::vec2 hitPos(xHit, yHit);
            float distanceTravelledBeforeHit = glm::distance(origin, hitPos);

            if(functor(currentCell, {directionSigns.x, 0}, hitPos, distanceTravelledBeforeHit))
                break;
        }
        else
        {
            distanceToNextHit.y = distanceToNextHit.y + rayDeltaBetweenBoundaries.y;
            currentCell.y += directionSigns.y;

            float yHit = currentCell.y * cellSize.y + (directionSigns.y < 0.0f ? cellSize.y : 0.0f);
            float xHit = (yHit - lineConstant) / lineCoefficient;

            glm::vec2 hitPos(xHit, yHit);
            float distanceTravelledBeforeHit = glm::distance(origin, hitPos);

            if(functor(currentCell, {0, directionSigns.y}, hitPos, distanceTravelledBeforeHit))
                break;
        }
    }
}

std::vector<LandscapeCollision> moveAgainstLandscape(struct GameData& data);