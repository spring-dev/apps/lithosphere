#include "updatelandscapeattach.hpp"
#include <gamedata.hpp>
#include <dpx/join.hpp>
#include <dpx/disable.hpp>
#include <spr/data/position.hpp>
#include <data/landscapeattach.hpp>
#include <data/physicalvertices.hpp>
#include <spr/debug/debugrenderer.hpp>

void updateLandscapeAttach(struct GameData& data)
{
    constexpr float padding = 0.5f;

    dpx::join([&](dpx::TableId id, LandscapeAttach& attach, spr::Position& position, const PhysicalVertices& vertices)
    {
        WorldCoordinate entityPos = spr::swizzle("xy", position.coordinate);
        glm::vec2 vertexOffset = vertices.coordinates[attach.vertex];
        WorldCoordinate currentVertexPos = entityPos + vertexOffset;

        //spr::DRen::out << spr::DebugMark{.position=currentVertexPos, .width=5.0f, .color=spr::Color::Yellow(), .ttl=1};

        //TODO: make work up and down
        //TODO: make work for horizontal case

        float checkRangeBegin = currentVertexPos.y - attach.tolerance;
        float checkRangeEnd = currentVertexPos.y + attach.tolerance;

        UnitCoordinate currentCell = convertTo<UnitCoordinate>(WorldCoordinate(currentVertexPos));
        UnitCoordinate cellBegin = convertTo<UnitCoordinate>(WorldCoordinate(currentVertexPos.x, checkRangeBegin));
        UnitCoordinate cellEnd = convertTo<UnitCoordinate>(WorldCoordinate(currentVertexPos.x, checkRangeEnd));

        std::optional<int> solidCellY;
        for(int y = cellBegin.y; y < cellEnd.y + 1; ++y)
        {
            UnitCoordinate cell{cellBegin.x, y};

            bool solid = data.lithosData.types.atOr(cell, LithosType::Air);

            if(solid)
            {
                solidCellY = y;
                break;
            }
        }

        if(solidCellY)
        {
            //we can now work out the target pos
            WorldCoordinate solidWorld = convertTo<WorldCoordinate>(UnitCoordinate{currentCell.x, *solidCellY});
            WorldCoordinate targetPos = solidWorld + WorldCoordinate{0.0f, -padding};

            float yMoveDelta = targetPos.y - currentVertexPos.y;

            entityPos = entityPos + glm::vec2(0.0f, yMoveDelta);

            position.coordinate = glm::vec3(entityPos, position.coordinate.z);
        }

        attach.didAttach = solidCellY.has_value();

    }, *data.worldScene->game.tLandscapeAttach, *data.worldScene->spr.tPosition, *data.worldScene->game.tPhysicalVertices);
}