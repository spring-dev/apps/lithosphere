#pragma once
#include <dpx/tableid.hpp>

struct HudData
{
    dpx::TableId hudInvertShader;
    dpx::TableId crosshairSprite;
};
