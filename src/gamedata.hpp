#pragma once

#include <cstdint>
#include <map>
#include <random>
#include <unordered_set>

#include <dpx/tables.hpp>
#include <dpx/tableid.hpp>

#include <spr/data/tables.hpp>
#include <spr/physics/quadtree.hpp>
#include <spr/frame/framelogic.hpp>
#include <spr/input/inputlogic.hpp>
#include <spr/physics/collisionlogic.hpp>
#include <spr/spatial/layeredgrid.hpp>
#include <spr/noise/noise.hpp>

#include <constants/constants.hpp>
#include <data/tables.hpp>
#include <player/playeraction.hpp>
#include <player/playerdata.hpp>
#include <rendering/renderlogic.hpp>
#include <startupconstants.hpp>

#include <scene/scene.hpp>
#include <lithos/lithos.hpp>

#include <graphics/huddata.hpp>

struct LayersConfig
{
    constexpr static std::array<spr::LayerConfig, 3> array
    {{
        {
            32,
        },
        {
            128,
        },
        {
            1024,
        }
    }};
};

using CollisionGrid = spr::LayeredGrid<dpx::TableId, LayersConfig>;

struct GameData
{
    GameData();
    ~GameData();
    //global tables
    spr::GlobalTables sprg;
    gam::GlobalTables gamg;
    //scene
    std::unique_ptr<Scene> worldScene;
    std::unique_ptr<Scene> uiScene;

    //gameplay constants
    std::unique_ptr<constants::Constants> c = constants::makeConstants();

    //system
    glm::ivec2 screenSize = cInitialScreenSize;
    bool paused = false;
    int32_t advancePaused = 0;
    bool showDebugMenu = false;
    bool showTables = false;
    bool showProfiler = false;
    bool showMouse = true;

    //logicdata
    spr::InputLogic::Data inputData;
    spr::FrameLogic::Data frameData;
    RenderLogic::Data renderData;
    spr::CollisionLogic::Data collisionData;
    PlayerData playerData;


    //entities
    std::unordered_set<dpx::TableId> entitiesToRemove;
    CollisionGrid spatialEntityStorage;

    //lithosphere
    LithosData lithosData;
    
    //gfx
    HudData hudData;
    int zoomFactor = 100;

    //random
    std::mt19937 randomEngine;
    spr::NoisePermutationTable noisePerm;

    //debug
    struct E : std::vector<std::string>
    {
        void operator()(const std::string& s)
        {
            line += s;

            if(!line.empty() && line.back() == '\n')
            {
                push_back(std::move(line));
                line.clear();
            }
        }

        std::string s(glm::vec2 v)
        {
            char b[64];

            sprintf(b, "%.2f,%.2f", v.x, v.y);

            return b;
        }

        std::string line;
    } events;
};
