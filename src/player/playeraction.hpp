#pragma once

#include <spr/hash/stringhash.hpp>

namespace PlayerAction
{
    constexpr spr::StringHash Left = "player_left"_hash;
    constexpr spr::StringHash Right = "player_right"_hash;
    constexpr spr::StringHash Up = "player_up"_hash;
    constexpr spr::StringHash Down = "player_down"_hash;
    constexpr spr::StringHash Jump = "player_jump"_hash;
    constexpr spr::StringHash Throw = "player_throw"_hash;
    constexpr spr::StringHash Dig = "player_dif"_hash;
}