#include "playerintelligence.hpp"
#include <thero/enumerate.hpp>
#include <gamedata.hpp>
#include "data/playerintelligence.hpp"
#include "data/moveintention.hpp"
#include "data/jumpintention.hpp"
#include "data/digintention.hpp"
#include <utils.hpp>
#include <dpx/assign.hpp>

void processPlayerIntelligence(GameData& data)
{
    dpx::forEach([&](dpx::TableId id, const PlayerIntelligence& intelligence)
    {
        //move intention
        std::optional<MoveIntention> moveIntention;

        if(spr::isActionOngoing(PlayerAction::Left, data.inputData))
            materialize(moveIntention).direction.x += -1.0f;
        if(spr::isActionOngoing(PlayerAction::Right, data.inputData))
            materialize(moveIntention).direction.x += 1.0f;

        if(moveIntention && glm::length(moveIntention->direction))
        {
            moveIntention->direction = glm::normalize(moveIntention->direction);
            moveIntention->speedFactor = 1.0f;
        }
        assign(id, moveIntention, *data.worldScene->game.tMoveIntention);

        //jump intention
        std::optional<JumpIntention> jumpIntention;

        if(spr::isActionOngoing(PlayerAction::Jump, data.inputData))
            materialize(jumpIntention).strengthFactor = 1.0f;
        assign(id, jumpIntention, *data.worldScene->game.tJumpIntention);

        //dig intention
        std::optional<DigIntention> digIntention;

        if(spr::isActionOngoing(PlayerAction::Dig, data.inputData))
            materialize(digIntention).speedFactor = 1.0f;
        assign(id, digIntention, *data.worldScene->game.tDigIntention);

    }, *data.worldScene->game.tPlayerIntelligence);
}
