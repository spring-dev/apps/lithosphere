#pragma once

struct GameData;

namespace scenarios
{
    void testScenario(GameData& data);
}
