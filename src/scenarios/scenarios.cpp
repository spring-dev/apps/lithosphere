#include "scenarios.hpp"
#include <gamedata.hpp>
#include <spr/random/random.hpp>
#include <spr/entity/spawnentity.hpp>
#include <entity/entityutil.hpp>
#include <spr/resources/texture.hpp>
#include <spr/resources/shader.hpp>
#include <spr/resources/animationset.hpp>
#include <data/environmentalforces.hpp>
#include <entity/camerafollow.hpp>
#include <spr/data/physics.hpp>
#include <data/groundlocomotor.hpp>
#include <data/moveintention.hpp>
#include <data/physicalvertices.hpp>
#include <physics/collidecode.hpp>
#include <data/landscapecollider.hpp>
#include <data/clonkbehavior.hpp>
#include <data/playerintelligence.hpp>
#include <data/horizontalorientation.hpp>
#include <spr/resources/animation.hpp>

namespace scenarios
{
    void createLand(GameData& data)
    {
        glm::ivec2 mapSize{5000, 3000};
        UnitCoordinate start = convertTo<UnitCoordinate>(WorldCoordinate{0, 0});
        UnitCoordinate end = convertTo<UnitCoordinate>(WorldCoordinate{mapSize});

        auto ySolidAt = [] (float xPerc, float period, float amplitude, float height)
        {
            return std::sin(xPerc * period) / amplitude + height;
        };

        data.lithosData = createLithosData(mapSize);

        UnitCoordinate current = start;
        for(; current.x < end.x; ++current.x)
        {
            float worldX = convertTo<WorldCoordinate>(current).x;
            float worldXPerc = worldX / static_cast<float>(end.x - start.x);
            float solidPerc = ySolidAt(worldXPerc, 9.7f, 5.6f, 0.5f);

            for(current.y = start.y; current.y < end.y; ++current.y)
            {
                float worldY = convertTo<WorldCoordinate>(current).y;

                float worldYPerc = 1.0f - (worldY / static_cast<float>(end.y - start.y));

                if(worldYPerc <= solidPerc)
                {
                    data.lithosData.types.set(current, LithosType::Soil);
                    data.lithosData.colorVariation.set(current, static_cast<int8_t>(spr::randomIntRange(0, 3, data.randomEngine)));
                }
            }
        }

        current = start;
        for(; current.x < end.x; ++current.x)
        {
            float worldX = convertTo<WorldCoordinate>(current).x;
            float worldXPerc = worldX / static_cast<float>(end.x - start.x);
            float solidPerc = ySolidAt(worldXPerc, 19.7f, 10.6f, 0.3f);

            for(current.y = start.y; current.y < end.y; ++current.y)
            {
                float worldY = convertTo<WorldCoordinate>(current).y;

                float worldYPerc = 1.0f - (worldY / static_cast<float>(end.y - start.y));

                if(worldYPerc <= solidPerc)
                {
                    data.lithosData.types.set(current, LithosType::Granite);
                    data.lithosData.colorVariation.set(current, static_cast<int8_t>(spr::randomIntRange(0, 3, data.randomEngine)));
                }
            }
        }
    }

    constexpr float cEntityDepth = 10.0f;

    void spawnClonk(GameData& data, glm::vec2 pos)
    {
        auto properties = spr::createSetAnimatedSpriteProperties(glm::vec3(pos, cEntityDepth), {}, {}, {1.0f, 1.0f}, spr::getTexture("assets.clonk_texture"_hash, data.sprg), spr::getAnimation("assets.clonk_anims::idle"_hash, data.sprg), spr::getAnimationSet("assets.clonk_anims"_hash, data.sprg), spr::getShader("assets.sprite_shader"_hash, data.sprg));

        spr::addProperties(properties,
            spr::Physics{},
            EnvironmentalForces
            {
                .gravityForce = glm::vec2(0.0f, 0.25f),
            },
            GroundLocomotor
            {
                .maxAcceleration = 0.1f,
                .maxVelocity = 1.0f,
            },
            spr::Physics
            {

            },
            PhysicalVertices
            {
             //VertexX=0,0,0,-2,2,-4,4
             //VertexY=2,-7,9,-3,-3,3,3
                .coordinates={glm::vec2{0.0f, 2.0f}, glm::vec2{0.0f, -7.0f}, glm::vec2{0.0f, 9.0f}, glm::vec2{-2.0f, -3.0f}, glm::vec2{2.0f -3.0f}, glm::vec2{-4.0f, 3.0f}, glm::vec2{4.0f, 3.0f}},
                .codes=      { CollideCode::None,    CollideCode::Top,       CollideCode::Bottom,    CollideCode::Left,       CollideCode::Right,    CollideCode::Left,      CollideCode::Right,  }
            },
            LandscapeCollider
            {
                .stopMovement=true,
            },
            HorizontalOrientation{},
            ClonkBehavior{},
            PlayerIntelligence
            {
            .playerIndex=0
            }
        );

        dpx::TableId clonkId = addEntity(properties, *data.worldScene, data);

        //make camera follow
        setCameraFollow(data.renderData.worldCameras[data.renderData.observingWorldCamera], clonkId, data);
    }

    void testScenario(GameData& data)
    {
        createLand(data);
        spawnClonk(data, {100.0f, -100.0f});
    }
}
