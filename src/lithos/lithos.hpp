#pragma once
#include <lithos/discretefield.hpp>
#include <spr/color/color.hpp>
#include <array>

enum LithosType { Air, Soil, Granite };

constexpr std::array<std::array<spr::Color, 4>, 3> cLithosColors =
{{
    {{ //Air
    }},
    {{ //Soil
        spr::Color(224, 160, 88),
        spr::Color(200, 128, 56),
        spr::Color(176, 96, 24),
        spr::Color(248, 192, 120),
    }},
    {{ //Granite
        spr::Color(35, 36, 41),
        spr::Color(116, 112, 111),
        spr::Color(159, 159, 161),
        spr::Color(208, 205, 195),
    }},
}};

struct LithosData
{
    glm::ivec2 size;
    DiscreteGrid<LithosType> types;
    DiscreteGrid<int8_t> colorVariation;
};


inline LithosData createLithosData(glm::ivec2 size)
{
    return
    {
        size,
        createGrid<LithosType>(size),
        createGrid<int8_t>(size),
    };
}
