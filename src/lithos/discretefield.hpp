#pragma once
#include <ska/flat_hash_map.hpp>
#include <stc/static_vector.hpp>
#include <spr/glm.hpp>

template <typename tag, typename underlying>
struct TCoordinate2 : underlying
{
    using UnderlyingType = underlying;
    using ValueType = std::decay_t<decltype(underlying().x)>;

    using UnderlyingType::UnderlyingType;
    constexpr TCoordinate2(UnderlyingType other): underlying(other.x, other.y)
    {
    }

    template <typename otherTag, typename otherUnderlying>
    constexpr TCoordinate2(TCoordinate2<otherTag, otherUnderlying> other) = delete;

    UnderlyingType glm() const { return *this; }

    bool operator<(const TCoordinate2& other) const
    {
        stc::static_vector<ValueType, 2> av {{this->x, this->y}};
        stc::static_vector<ValueType, 2> bv {{other.x, other.y}};

        return av < bv;
    }
    bool operator>(const TCoordinate2& other) const
    {
        return stc::static_vector<ValueType, 2>{glm().x, glm().y} > stc::static_vector<ValueType, 2>{other.x, other.y};//return glm() < other.glm();
    }
    bool operator>=(const TCoordinate2& other) const
    {
        return *this == other || *this > other;
    }
    bool operator<=(const TCoordinate2& other) const
    {
        return *this == other || *this < other;
    }
};

template <typename tag>
using ICoordinate2 = TCoordinate2<tag, glm::ivec2>;
template <typename tag>
using FCoordinate2 = TCoordinate2<tag, glm::vec2>;

namespace std
{
    template<typename tag> struct hash<ICoordinate2<tag>>
    {
        typedef ICoordinate2<tag> argument_type;
        typedef std::size_t result_type;
        constexpr result_type operator()(argument_type const& s) const noexcept
        {
            return std::hash<glm::ivec2>()(s);
        }
    };
}

using UnitCoordinate = ICoordinate2<struct UnitCoordinate_tag>;
using WorldCoordinate = FCoordinate2<struct WorldCoordinate_tag>;
using WorldICoordinate = ICoordinate2<struct WorldICoordinate_tag>;

template <typename TargetCoordType, typename... SourceCoordType>
constexpr TargetCoordType convertTo(SourceCoordType...)
{
    static_assert(sizeof(TargetCoordType) != sizeof(TargetCoordType), "function must be specialized for these types");
    return {};
}

template<> //world coord to unit coord
inline UnitCoordinate convertTo<UnitCoordinate, WorldCoordinate>(WorldCoordinate coord)
{
    return UnitCoordinate(coord.x, coord.y);
}

template<> //unit coord to world coord
constexpr WorldCoordinate convertTo<WorldCoordinate, UnitCoordinate>(UnitCoordinate coord)
{
    return WorldCoordinate(coord.x, coord.y);
}

/*
template <typename ValueType, typename CoordType, size_t width>
struct DataGrid : std::array<ValueType, width*width>
{
    DataGrid()
    {
        this->fill({});
    }

    DataGrid(const ValueType& val)
    {
        this->fill(val);
    }

    DataGrid(std::initializer_list<ValueType>) = delete;

    const ValueType& at(CoordType coord) const
    {
        return (*this)[toIndex(coord)];
    }

    ValueType& at(CoordType coord)
    {
        return (*this)[toIndex(coord)];
    }

    ValueType& set(CoordType coord, ValueType const &  val)
    {
        return(at(coord) = val);
    }

    ValueType& set(CoordType coord, ValueType&&  val)
    {
        return(at(coord) = std::move(val));
    }

    static constexpr size_t toIndex(CoordType coord)
    {
        return coord.x + coord.y * width;
    }
};

template <typename ValueType>
struct DiscreteField
{
    using ChunkType = DataGrid<ValueType, ChunkLocalCoordinate, cChunkWidth>;
    ska::flat_hash_map<ChunkCoordinate, ChunkType> chunks;

    //chunks

    const ChunkType* find(ChunkCoordinate coord) const
    {
        auto chunk = chunks.find(coord);
        return chunk == chunks.end() ? nullptr : &chunk->second;
    }

    ChunkType* find(ChunkCoordinate coord)
    {
        const ValueType* res = const_cast<DiscreteField const *>(this)->find(coord);
        return const_cast<ValueType*>(res);
    }

    const ChunkType& at(ChunkCoordinate coord) const
    {
        return *find(coord);
    }

    ChunkType& at(ChunkCoordinate coord)
    {
        return *find(coord);
    }

    //units

    const ValueType* find(UnitCoordinate coord) const
    {
        auto coords = convertTo<ChunkAndLocalCoordinate>(coord);
        auto chunk = chunks.find(coords.chunk);
        return chunk == chunks.end() ? nullptr : &chunk->second.at(coords.local);
    }

    ValueType* find(UnitCoordinate coord)
    {
        const ValueType* res = const_cast<DiscreteField const *>(this)->find(coord);
        return const_cast<ValueType*>(res);
    }

    const ValueType& at(UnitCoordinate coord) const
    {
        return *find(coord);
    }

    ValueType& at(UnitCoordinate coord)
    {
        return *find(coord);
    }

    ValueType atOr(UnitCoordinate coord, ValueType defaultVal) const
    {
        const ValueType* res = find(coord);
        return res ? *res : defaultVal;
    }

    ValueType& set(UnitCoordinate coord, const ValueType& val)
    {
        auto coords = convertTo<ChunkAndLocalCoordinate>(coord);
        auto& chunk = chunks[coords.chunk];
        return chunk.at(coords.local) = val;
    }

    ValueType& set(UnitCoordinate coord, ValueType&& val)
    {
        auto coords = convertTo<ChunkAndLocalCoordinate>(coord);
        auto& chunk = chunks[coords.chunk];
        return chunk.at(coords.local) = std::move(val);
    }
};
*/

template <typename ValueType>
struct DiscreteGrid
{
    inline const ValueType& at(UnitCoordinate coord) const
    {
        return data[toIndex(coord, size.x)];
    }

    inline ValueType& at(UnitCoordinate coord)
    {
        return data[toIndex(coord, size.x)];
    }

    inline ValueType atOr(UnitCoordinate coord, ValueType defaultVal) const
    {
        if(inBounds(coord))
        {
            size_t i = toIndex(coord, size.x);
            return i < data.size() ? data[i] : defaultVal;
        }
        else
        {
            return defaultVal;
        }
    }

    inline ValueType& set(UnitCoordinate coord, ValueType val)
    {
        return data[toIndex(coord, size.x)] = val;
    }

    inline bool setOrNoop(UnitCoordinate coord, ValueType val)
    {
        if (inBounds(coord))
        {
            data[toIndex(coord, size.x)] = val;
            return true;
        }
        else
            return false;
    }

    static constexpr size_t toIndex(UnitCoordinate coord, int width)
    {
        return coord.x + coord.y * width;
    }

    bool inBounds(UnitCoordinate coord) const
    {
        return coord.x > 0 && coord.y > 0 && coord.x < size.x && coord.y < size.y;
    }

    glm::ivec2 size;
    std::vector<ValueType> data;
};

template <typename ValueType>
inline DiscreteGrid<ValueType> createGrid(glm::ivec2 size)
{
    return {size, std::vector<ValueType>(static_cast<size_t>(size.x * size.y), {})};
}
