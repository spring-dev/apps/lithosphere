#pragma once
#include <spr/glm.hpp>

template <typename PaintFunc>
void paintCircle(glm::vec2 position, float radius, PaintFunc paintFunc)
{
    glm::ivec2 mid = static_cast<glm::ivec2>(position);
    glm::ivec2 start = mid - glm::ivec2(radius, radius);
    glm::ivec2 end = mid + glm::ivec2(radius + 1, radius + 1);

    for(int y = start.y; y < end.y; ++y)
    {
        for(int x = start.x; x < end.x; ++x)
        {
            glm::vec2 current(x, y);
            float distance = glm::distance(current, position);
            if(distance <= radius)
            {
                float perc = distance / radius;
                int magnitude = static_cast<int>((1.0f - perc) / 0.25);
                paintFunc(current, magnitude);
            }
        }
    }
}