#pragma once

template <typename T>
T& materialize(std::optional<T>& opt, T init = {})
{
    if(!opt)
        opt = std::move(init);
    return *opt;
}

template <typename T>
const T& materialize(const std::optional<T>& opt, T init = {})
{
    if(!opt)
        opt = std::move(init);
    return *opt;
}