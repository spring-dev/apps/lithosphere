#pragma once
#include <spr/glm.hpp>
#include <spr/color/color.hpp>
#include <spr/rendering/spritequadbuilder.hpp>
#include <spr/rendering/imguirenderer.hpp>
#include <spr/rendering/textrenderer.hpp>
#include <spr/rendering/quadrenderer.hpp>
#include <rendering/landscaperenderer.hpp>

struct GameData;

class RenderLogic
{
    public:
        struct Data
        {
            dpx::TableId mainViewport = dpx::Null;
            std::vector<dpx::TableId> worldCameras;
            size_t observingWorldCamera;
            size_t cullingWorldCamera;
            dpx::TableId guiCamera = dpx::Null;

            //landscape
            std::vector<UnitCoordinate> invalidatedUnits;

            inline std::pair<dpx::TableId, dpx::TableId> getObservingViewportAndCamera() { return {mainViewport, worldCameras[observingWorldCamera]}; }
            inline void invalidateUnit(UnitCoordinate unit) { invalidatedUnits.push_back(unit); }
        };
        RenderLogic(Data& renderData, GameData& data);
        void frameStart(spr::Color clearColor = spr::Color::Black());
        void renderFrame();
        void resizeWindow(glm::ivec2 size);
    private:
        Data& mRenderData;
        GameData& mData;
        spr::SpriteQuadBuilder mSpriteQuadBuilder;
        spr::TextRenderer mTextRenderer;
        spr::ImguiRenderer mImguiRenderer;
        std::vector<spr::QuadInfo> mQuadStorage;
        spr::QuadRenderer mQuadRenderer;
        LandscapeRenderer mLandscapeRenderer;
};
