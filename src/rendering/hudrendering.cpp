#include "hudrendering.hpp"
#include <spr/entity/spawnentity.hpp>
#include <spr/gl/shader.hpp>
#include <gamedata.hpp>
#include <entity/entityutil.hpp>
#include <spr/resources/texture.hpp>
#include <spr/resources/shader.hpp>
#include <spr/data/position.hpp>
#include <spr/gl/viewport.hpp>
#include <spr/camera/camera.hpp>
#include <debug/debugrendering.hpp>
#include <imgui/imgui.h>

void setupHud(GameData& data)
{
    auto halfScreenSize = data.screenSize / 2;
    spr::EntityProperties props = spr::createSpriteProperties(glm::vec3(static_cast<glm::vec2>(halfScreenSize), 0.0f), {}, {}, {1.0, 1.0}, {spr::getTexture("assets.crosshair"_hash, data.sprg)}, spr::getShader("assets.sprite_shader"_hash, data.sprg));
    data.hudData.crosshairSprite = addEntity(props, *data.uiScene, data);
}

void setCrosshairPosition(GameData& data)
{
    glm::vec3& position = get(data.hudData.crosshairSprite, *data.uiScene->spr.tPosition).coordinate;

    auto halfScreenSize = data.screenSize / 2;

    position = glm::vec3(static_cast<glm::vec2>(halfScreenSize), 0.0f);
}

void renderPixel(WorldCoordinate world, GameData& data)
{
    UnitCoordinate unit = convertTo<UnitCoordinate>(world);
    WorldCoordinate worldSnapped = convertTo<WorldCoordinate>(unit);

    spr::DRen::out << spr::DRen::make(spr::DebugFrame{.start=worldSnapped, .size={1.0f, 1.0f}}, spr::Color::Blue(), 1, DebugCode::render);
}

void renderMouseInfoWindow(glm::ivec2 screenPos, WorldCoordinate worldPos, GameData& data)
{
    glm::vec2 windowPos = static_cast<glm::vec2>(data.screenSize) - glm::vec2(150.0f, 150.0f);
    UnitCoordinate unit = convertTo<UnitCoordinate>(worldPos);

    ImGui::SetNextWindowPos(glm::vec2{windowPos.x, windowPos.y});
    ImGui::Begin("Cursor");
    ImGui::Text("Unit: %d %d", unit.x, unit.y);
    ImGui::End();
}

void renderMouseInfo(GameData& data)
{
    auto [viewportId, cameraId] = data.renderData.getObservingViewportAndCamera();
    WorldCoordinate mouseWorld = spr::screenToWorld(viewportId, cameraId, data.inputData.mousePosition, data.sprg);

    renderPixel(mouseWorld, data);
    renderMouseInfoWindow(data.inputData.mousePosition, mouseWorld, data);
}

void renderEventLog(GameData& data)
{
    glm::vec2 windowPos = glm::vec2(data.screenSize.x, 0.0f) + glm::vec2(-750.0f, 50.0f);

    ImGui::SetNextWindowPos(glm::vec2{windowPos.x, windowPos.y});
    ImGui::Begin("Events");
    if(ImGui::SmallButton("clear"))
        data.events.clear();
    for(const auto& event : data.events)
        ImGui::Text("%s", event.c_str());
    ImGui::End();
}

void renderHud(GameData& data)
{
    renderMouseInfo(data);
    renderEventLog(data);
    return;

    static bool tempHack = false;
    if(!tempHack)
    {
        setupHud(data);
        tempHack = true;
    }

    setCrosshairPosition(data);
}
