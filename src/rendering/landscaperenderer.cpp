#include "landscaperenderer.hpp"
#include <dpx/tableid.hpp>
#include <dpx/join.hpp>
#include <dpx/count.hpp>
#include <spr/gl/gl.hpp>
#include <spr/gl/vbo.hpp>
#include <spr/gl/vao.hpp>
#include <spr/gl/shader.hpp>
#include <spr/gl/texture.hpp>
#include <spr/gl/viewport.hpp>
#include <spr/camera/camera.hpp>
#include <spr/data/tables.hpp>
#include <spr/data/gltexture.hpp>
#include <spr/data/camera.hpp>

#include <spr/data/viewport.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <tracy.hpp>
#include <lithos/lithos.hpp>
#include <spr/random/random.hpp>
#include <range/v3/all.hpp>

LandscapeRenderer::LandscapeRenderer(spr::GlobalTables& gTables):
    mGTables(gTables), mSize(1, 1)
{
    mPosVbo = createVbo(*mGTables.tVertexBufferObject);
    mTexCoordVbo = createVbo(*mGTables.tVertexBufferObject);
    mColorVbo = createVbo(*mGTables.tVertexBufferObject);

    mVao = createVao(
    {
        {mPosVbo, 0, {3, GL_FLOAT, false, 0, 0}},
        {mTexCoordVbo, 1, {2, GL_FLOAT, false, 0, 0}},
        {mColorVbo, 2, {4, GL_FLOAT, false, 0, 0}},
    }, mGTables);

    mLandscapeTexture = spr::gl::createTexture({cDrawChunkWidth, cDrawChunkWidth}, spr::Color::Orange(), gTables);
}

void LandscapeRenderer::render(dpx::TableId viewportId, dpx::TableId cameraId, dpx::TableId shaderId, const LithosData& lithosData)
{
    if(mSize.x < 2 && mSize.y < 2)
        return;

    bindVao(mVao, mGTables);

    std::vector<glm::vec3> positions(6);
    std::vector<glm::vec2> texCoords(6);
    std::vector<glm::vec4> colors(6);

    texCoords[0] = {0.0, 0.0f};
    texCoords[1] = {0.0, 1.0f};
    texCoords[2] = {1.0, 1.0f};

    texCoords[3] = {0.0, 0.0f};
    texCoords[4] = {1.0, 1.0f};
    texCoords[5] = {1.0, 0.0f};

    colors[0] = {1.0f, 1.0f, 1.0f, 1.0f};
    colors[1] = {1.0f, 1.0f, 1.0f, 1.0f};
    colors[2] = {1.0f, 1.0f, 1.0f, 1.0f};
    colors[3] = {1.0f, 1.0f, 1.0f, 1.0f};
    colors[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    colors[5] = {1.0f, 1.0f, 1.0f, 1.0f};

    setArrayVboData(mTexCoordVbo, GL_STREAM_DRAW, texCoords.data(), texCoords.size() * sizeof(texCoords[0]), *mGTables.tVertexBufferObject);
    setArrayVboData(mColorVbo, GL_STREAM_DRAW, colors.data(), colors.size() * sizeof(colors[0]), *mGTables.tVertexBufferObject);
  
    //set viewport and camera
    spr::gl::setViewport(viewportId, mGTables);
    glm::mat4 viewProjection = viewProjectionMatrix(cameraId, mGTables);

    //set shader
    bindShader(shaderId, mGTables);
    int32_t projectionMatrixLocation = spr::uniformLocation(shaderId, "viewProjection", mGTables);
    glUniformMatrix4fv(projectionMatrixLocation, 1, GL_FALSE, &viewProjection[0][0]);

    //set texture. will be bound in loop below
    int32_t textureLocation = spr::uniformLocation(shaderId, "texture", mGTables);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, get(mLandscapeTexture, *mGTables.tGlTexture).glId);
    glUniform1i(textureLocation, 0);

    glm::vec2 worldStart = spr::screenToWorld(viewportId, cameraId, {0, 0}, mGTables);
    glm::vec2 worldLast = spr::screenToWorld(viewportId, cameraId, {mSize - glm::ivec2(1, 1)}, mGTables);

    DrawChunkCoordinate chStart = convertTo<DrawChunkCoordinate>(convertTo<UnitCoordinate>(WorldCoordinate(worldStart)));
    DrawChunkCoordinate chLast = convertTo<DrawChunkCoordinate>(convertTo<UnitCoordinate>(WorldCoordinate(worldLast)));

    DrawChunkCoordinate currentDrawChunkCoord;
    for(currentDrawChunkCoord.y = chStart.y; currentDrawChunkCoord.y <= chLast.y; ++currentDrawChunkCoord.y)
    {
        if(currentDrawChunkCoord.y < 0)
            continue;
        for(currentDrawChunkCoord.x = chStart.x; currentDrawChunkCoord.x <= chLast.x; ++currentDrawChunkCoord.x)
        {
            if(currentDrawChunkCoord.x < 0)
                continue;

            WorldCoordinate currentCoord = convertTo<WorldCoordinate>(convertTo<UnitCoordinate>(currentDrawChunkCoord));
            WorldCoordinate chunkSize = {cDrawChunkWidth, cDrawChunkWidth};
            WorldCoordinate start = WorldCoordinate{currentCoord};
            WorldCoordinate end = WorldCoordinate{start + chunkSize};

            positions[0] = {start.x, start.y, 0.5f};
            positions[1] = {start.x, end.y, 0.5f};
            positions[2] = {end.x, end.y, 0.5f};

            positions[3] = {start.x, start.y, 0.5f};
            positions[4] = {end.x, end.y, 0.5f};
            positions[5] = {end.x, start.y, 0.5f};

            const spr::Image& chunkImage = getOrDrawChunkImage(currentDrawChunkCoord, lithosData);

            if(chunkImage.pixels.empty())
                continue;

            spr::gl::updatePart(mLandscapeTexture, {0, 0}, chunkImage, mGTables);

            setArrayVboData(mPosVbo, GL_STREAM_DRAW, positions.data(), positions.size() * sizeof(positions[0]), *mGTables.tVertexBufferObject);

            glDrawArrays(GL_TRIANGLES, 0, static_cast<int>(positions.size()));
        }
    }

    unbindVao(mGTables);
}

void LandscapeRenderer::resizeWindow(glm::ivec2 iSize)
{
    mSize = iSize;
}

void LandscapeRenderer::invalidate(const std::vector<UnitCoordinate>& units)
{
    if(units.empty())
        return;

    auto unitsToChunks = [] (const std::vector<UnitCoordinate>& units)
    {
        return units
            | ranges::views::transform(convertTo<DrawChunkCoordinate, UnitCoordinate>)
            | ranges::to_vector
            | ranges::actions::sort
            | ranges::actions::unique;
    };

    for(auto chunk : unitsToChunks(units))
        mChunkImages.erase(chunk);
}

const spr::Image& LandscapeRenderer::getOrDrawChunkImage(DrawChunkCoordinate coordinate, const LithosData& lithosData)
{
    auto imageIter = mChunkImages.find(coordinate);

    if(imageIter != mChunkImages.end())
    {
        return imageIter->second;
    }

    else
    {
        spr::Image& image = (mChunkImages[coordinate] = spr::createImage({cDrawChunkWidth, cDrawChunkWidth}, spr::Color{55, 119, 190}));

        //generate chunk image

        UnitCoordinate startCoord = convertTo<UnitCoordinate, DrawChunkCoordinate>(coordinate);
        UnitCoordinate endCoord{startCoord + UnitCoordinate{cDrawChunkWidth, cDrawChunkWidth}};
        UnitCoordinate current = {};

        bool noneSet = true;

        for(current.y = startCoord.y; current.y < endCoord.y; ++current.y)
        {
            for(current.x = startCoord.x; current.x < endCoord.x; ++current.x)
            {
                LithosType type = lithosData.types.atOr(current, LithosType::Air);
                if(type != LithosType::Air)
                {
                    int8_t colorVariation = lithosData.colorVariation.at(current);

                    set(convertTo<DrawChunkLocalCoordinate, UnitCoordinate>(current), cLithosColors[type][colorVariation], image);
                    noneSet = false;
                }
            }
        }

        if(noneSet)
            image = {};

        return image;
    }
}
