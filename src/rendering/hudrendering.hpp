#pragma once
#include <dpx/tableid.hpp>

void setupHud(struct GameData& data);
void renderHud(struct GameData& data);
