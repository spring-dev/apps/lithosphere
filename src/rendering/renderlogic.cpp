#include "renderlogic.hpp"
#include <imgui/imgui.h>
#include <gamedata.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <spr/debug/debugrenderer.hpp>
#include <spr/data/gltexture.hpp>
#include <spr/data/camera.hpp>
#include <spr/data/viewport.hpp>
#include <spr/data/orthographicprojection.hpp>
#include <spr/data/perspectiveprojection.hpp>
#include <spr/gl/gl.hpp>
#include <spr/gl/viewport.hpp>
#include <spr/camera/camera.hpp>
#include <spr/gl/texture.hpp>
#include <spr/resources/shader.hpp>
#include <rendering/hudrendering.hpp>

#include <tracy.hpp>

constexpr glm::ivec2 cInitialSize = {1366, 768};

RenderLogic::RenderLogic(RenderLogic::Data& renderData, GameData& data):
    mRenderData(renderData),
    mData(data),
    mSpriteQuadBuilder(mData.sprg),
    mTextRenderer(mData.sprg),
    mImguiRenderer(mData.sprg),
    mQuadRenderer(mData.sprg),
    mLandscapeRenderer(mData.sprg)
{
    //setup cameras and views
    mRenderData.mainViewport = insert(spr::Viewport
    {
        {0, 0},
        cInitialSize,
    }, *mData.sprg.tViewport).id;

    mRenderData.guiCamera = insert(spr::Camera
    {
        {0.0f, 0.0f, 0.0f},
        glm::quat(1.0f, 0.0f, 0.0f, 0.0f),
    }, *mData.sprg.tCamera).id;
    insert(mRenderData.guiCamera, spr::OrthographicProjection
    {
        0.0f, //top
        0.0f, //left
        static_cast<float>(cInitialSize.y), //bottom
        static_cast<float>(cInitialSize.x),//right
        -100.0f,//near
        100.f,//far
    }, *mData.sprg.tOrthographicProjection);

    mRenderData.worldCameras =
    {
        insert(spr::Camera
        {
            {0.0f, 0.0f, 0.0f},
            glm::quat(1.0f, 0.0f, 0.0f, 0.0f),
        }, *mData.sprg.tCamera).id,
        insert(spr::Camera
        {
            {0.0f, 0.0f, 0.0f},
            glm::quat(1.0f, 0.0f, 0.0f, 0.0f),
        }, *mData.sprg.tCamera).id
    };

    insert(mRenderData.worldCameras[0], spr::OrthographicProjection
    {
        0.0f, //top
        0.0f, //left
        static_cast<float>(cInitialSize.y), //bottom
        static_cast<float>(cInitialSize.x),//right
        -100.0f,//near
        100.f,//far
    }, *mData.sprg.tOrthographicProjection);
    insert(mRenderData.worldCameras[1], spr::OrthographicProjection
    {
        0.0f, //top
        0.0f, //left
        static_cast<float>(cInitialSize.y), //bottom
        static_cast<float>(cInitialSize.x),//right
        -100.0f,//near
        100.f,//far
    }, *mData.sprg.tOrthographicProjection);

    mRenderData.observingWorldCamera = 0;
    mRenderData.cullingWorldCamera = 0;

    //initialize our global debug renderer
    spr::DRen::initialize(data.sprg);

    //setup gl values
    glEnable(GL_BLEND);

    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    resizeWindow(cInitialSize);
}

void RenderLogic::frameStart(spr::Color clearColor)
{
    glClearColor(clearColor.rAsFloat(), clearColor.gAsFloat(), clearColor.bAsFloat(), clearColor.aAsFloat());
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    spr::gl::setViewport(mRenderData.mainViewport, mData.sprg);

    mImguiRenderer.frameStart();
}

constexpr float cEntityDepthMin = 0.0f;
constexpr float cEntityDepthMax = 1.0f;

void RenderLogic::renderFrame()
{
    ZoneScoped;

    dpx::TableId viewport = mRenderData.mainViewport;

    dpx::TableId observingCam = mRenderData.worldCameras[mRenderData.observingWorldCamera];

    {
        ZoneScopedN("invalidating landscape")
        mLandscapeRenderer.invalidate(mRenderData.invalidatedUnits);
        mRenderData.invalidatedUnits.clear();
    }
    {
        ZoneScopedN("rendering landscape")
        mLandscapeRenderer.render(viewport, observingCam, spr::getShader("assets.sprite_shader"_hash, mData.sprg), mData.lithosData);
    }

    //clear quad storage, will not deallocate
    mQuadStorage.clear();

    //gather normalized depth values of all quads to be able to render in range properly
    spr::Normalization entityNormalization;

    //observingCam can be {} to not have it be camerafacing
    mSpriteQuadBuilder.accumulateZMinMax(observingCam, entityNormalization.sourceRangeMin, entityNormalization.sourceRangeMax, mData.worldScene->spr);
    //mParticleQuadBuilder.accumulateZMinMax(observingCam, entityNormalization.sourceRangeMin, entityNormalization.sourceRangeMax, mData.worldScene->spr);

    entityNormalization.targetRangeMin = cEntityDepthMin;
    entityNormalization.targetRangeMax = cEntityDepthMax;

    ////all world sprites
    {
        ZoneScopedN("gathering sprite quads")
        mSpriteQuadBuilder.buildQuads(observingCam, false, entityNormalization, mData.worldScene->spr, mQuadStorage);
    }
    {
        ZoneScopedN("rendering quads")
        mQuadRenderer.render(viewport, observingCam, false, mQuadStorage);
    }
    //all texts
    {
        ZoneScopedN("text renderer")
        mTextRenderer.render(viewport, observingCam, mData.worldScene->spr);
    }
    //draw all debug primitives
    {
        ZoneScopedN("debug renderer")
        spr::DRen::render(viewport, observingCam, spr::getShader("assets.sprite_shader"_hash, mData.sprg));
    }
    //HUD
    {
        ZoneScopedN("hud renderer")
        renderHud(mData);
    }
    //all gui sprites
    {
        //clear quad storage, will not deallocate
        mQuadStorage.clear();

        //gather normalized depth values of all quads to be able to render in range properly
        spr::Normalization entityNormalization;

        //observingCam can be {} to not have it be camerafacing
        mSpriteQuadBuilder.accumulateZMinMax(mRenderData.guiCamera, entityNormalization.sourceRangeMin, entityNormalization.sourceRangeMax, mData.uiScene->spr);

        entityNormalization.targetRangeMin = cEntityDepthMin;
        entityNormalization.targetRangeMax = cEntityDepthMax;

        ZoneScopedN("gathering gui sprite quads")
        mSpriteQuadBuilder.buildQuads(mRenderData.guiCamera, false, entityNormalization, mData.uiScene->spr, mQuadStorage);
    }
    {
        ZoneScopedN("rendering gui quads")
        mQuadRenderer.render(viewport, mRenderData.guiCamera, false, mQuadStorage);
    }
    //imgui
    {
        ZoneScopedN("imgui renderer")
        mImguiRenderer.renderFrame(viewport, mRenderData.guiCamera, spr::getShader("assets.sprite_shader"_hash, mData.sprg));
    }
}

void RenderLogic::resizeWindow(glm::ivec2 iSize)
{
    spr::gl::resizeViewport(mData.screenSize, mRenderData.mainViewport, mData.sprg);

    glm::vec2 size = static_cast<glm::vec2>(iSize);
    glm::vec2 halfSize = size / 2.0f;
    float zoom = static_cast<float>(mData.zoomFactor)/100.0f;
    glm::vec2 worldHalfSize = halfSize * zoom;

    for(dpx::TableId cam : mRenderData.worldCameras)
    {
        spr::OrthographicProjection& ortho = get(cam, *mData.sprg.tOrthographicProjection);
        ortho.left = -worldHalfSize.x;
        ortho.top = -worldHalfSize.y;
        ortho.right = worldHalfSize.x;
        ortho.bottom = worldHalfSize.y;
    }

    spr::OrthographicProjection& ortho = get(mRenderData.guiCamera, *mData.sprg.tOrthographicProjection);
    ortho.right = size.x;
    ortho.bottom = size.y;

    mLandscapeRenderer.resizeWindow(iSize);
}
