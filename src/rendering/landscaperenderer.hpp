#pragma once
#include <unordered_map>
#include <dpx/tableid.hpp>
#include <spr/glm.hpp>
#include <lithos/lithos.hpp>
#include <spr/image/image.hpp>

struct LithosData;

namespace spr
{
    struct GlobalTables;
}

using DrawChunkCoordinate = ICoordinate2<struct DrawChunkCoordinate_tag>;
using DrawChunkLocalCoordinate = ICoordinate2<struct DrawChunkLocalCoordinate_tag>;
constexpr size_t cDrawChunkWidth = 256;

template<> 
inline DrawChunkCoordinate convertTo<DrawChunkCoordinate, UnitCoordinate>(UnitCoordinate coord)
{
    return DrawChunkCoordinate(static_cast<glm::ivec2>(coord) / static_cast<int>(cDrawChunkWidth));
}

template<> 
inline DrawChunkLocalCoordinate convertTo<DrawChunkLocalCoordinate, UnitCoordinate>(UnitCoordinate coord)
{
    return DrawChunkLocalCoordinate(static_cast<glm::ivec2>(coord) % static_cast<int>(cDrawChunkWidth));
}

template<> 
inline UnitCoordinate convertTo<UnitCoordinate, DrawChunkCoordinate>(DrawChunkCoordinate coord)
{
    return UnitCoordinate(static_cast<glm::ivec2>(coord) * static_cast<int>(cDrawChunkWidth));
}

class LandscapeRenderer
{
    public:
        LandscapeRenderer(spr::GlobalTables& gTables);
        void render(dpx::TableId viewportId, dpx::TableId camera, dpx::TableId shaderId, const LithosData& lithosData);
        void resizeWindow(glm::ivec2 size);
        void invalidate(const std::vector<UnitCoordinate>& unit);
    private:
        const spr::Image& getOrDrawChunkImage(DrawChunkCoordinate coordinate, const LithosData& lithosData);
        spr::GlobalTables& mGTables;

        glm::ivec2 mSize;
        //texture storage 
        dpx::TableId mLandscapeTexture;

        //these are leaking now. make a wrapper?
        dpx::TableId mVao;
        dpx::TableId mPosVbo;
        dpx::TableId mTexCoordVbo;
        dpx::TableId mColorVbo;

        std::unordered_map<DrawChunkCoordinate, spr::Image> mChunkImages;
};
