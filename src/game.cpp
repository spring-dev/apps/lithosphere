#include "game.hpp"
#include <dpx/randomget.hpp>
#include <dpx/foreach.hpp>
#include <dpx/count.hpp>
#include <imgui/imgui.h>
#include <spr/data/tables.hpp>
#include <spr/debugguidata.hpp>
#include <spr/entitystates/stateutil.hpp>
#include <spr/input/bindingutil.hpp>
#include <spr/input/inputgroup.hpp>
#include <spr/debug/debugactions.hpp>
#include <spr/debug/debug.hpp>
#include <spr/debug/debugmenu.hpp>
#include <spr/resources/resourcemodule.hpp>
#include <spr/showdatatables.hpp>
#include <spr/gl/texture.hpp>
#include <spr/gl/viewport.hpp>
#include <input/globalaction.hpp>
#include <input/inputgroup.hpp>

#include <constants/allconstants.hpp>
#include <data/tables.hpp>
#include <debugguidata.hpp>
#include <entity/entityutil.hpp>
#include <entitystates/entitystates.hpp>
#include <spr/resources/animation.hpp>
#include <spr/resources/animationset.hpp>
#include <spr/resources/texture.hpp>
#include <spr/resources/font.hpp>
#include <spr/resources/audiosample.hpp>
#include <spr/resources/arraytexture.hpp>
#include <spr/resources/shader.hpp>
#include <showdatatables.hpp>
#include <tablecapacity.hpp>
#include <interface/updateinterface.hpp>
#include <entity/camerafollow.hpp>
#include <physics/environmentalforces.hpp>
#include <player/playerintelligence.hpp>
#include <locomotion/updatelocomotionforces.hpp>
#include <physics/acceleration.hpp>
#include <behaviors/clonkbehavior.hpp>
#include <physics/updatelandscapeattach.hpp>
#include <entity/entitylogic.hpp>
#include <entity/entitysprite.hpp>

#include <debug/debugrendering.hpp>

#include <tracy.hpp>

//const spr::GlContextSettings::Type contextType = spr::GlContextSettings::Type::ES;
const spr::GlContextSettings::Type contextType = spr::GlContextSettings::Type::CORE;

Game::Game() :
    //mWindow(cInitialScreenSize, "Game", {16, 0, 0, 2, 0, contextType}),
    mWindow(cInitialScreenSize, "Game", {16, 0, 0, 4, 3, contextType}),
    mFrameLogic(mData.frameData),
    mInputLogic(mData.inputData),
    mEntityStatesLogic(mData.sprg, mData.worldScene->spr),
    mPhysicsLogic(mData.frameData, mData.worldScene->spr),
    mCollisionLogic(mData.collisionData, mData.worldScene->spr),
    mRenderLogic(mData.renderData, mData),
    mAudioLogic(mData.sprg)
{
    mData.randomEngine.seed(std::random_device()());
    mData.noisePerm = spr::generateNoisePermutationTable(mData.randomEngine);

    mWindow.setVSync(true);

    //imgui
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    io.DisplaySize.x = static_cast<float>(mData.screenSize.x);
    io.DisplaySize.y = static_cast<float>(mData.screenSize.y);
    io.IniFilename = "data/imgui.ini";
    io.MousePos = {0, 0};
    io.DeltaTime = 1.0f/60.0f;
    unsigned char* pixels;
    int width, height;
    io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

    mImguiFontTexture = spr::gl::createTexture(glm::ivec2(width, height), pixels, mData.sprg);

    io.Fonts->TexID = reinterpret_cast<void*>(static_cast<uint64_t>(get(mImguiFontTexture, *mData.sprg.tGlTexture).glId));

    mInputLogic.mapImguiKeys(io.KeyMap);

    loadResources();

    mRenderLogic.resizeWindow(mData.screenSize);

    //setup logic settings
    mData.frameData.sFrameTimeMode = spr::FrameTimeMode::Fixed;

    startScenario();
}

Game::~Game()
{
    ImGui::DestroyContext();
}

void Game::loadResources()
{
    spr::sprEnsureCapacity(1024, mData.sprg);
    spr::sprEnsureCapacity(1024, mData.worldScene->spr);
    spr::sprEnsureCapacity(1024, mData.uiScene->spr);
    ensureCapacity(1024, mData.gamg);
    ensureCapacity(1024, mData.worldScene->game);
    ensureCapacity(1024, mData.uiScene->game);

    spr::scanResourceModules("assets", mData.sprg);

    //textures
    spr::loadAllTextures(mData.sprg);
   
    //animations
    spr::loadAllSpriteAnimations(mData.sprg);
   
    //animation sets
    spr::loadAllSpriteAnimationSets(mData.sprg);

    //fonts
    spr::loadAllFonts(mData.sprg);

    //audio
    //loadAndAddAudioSample("shoot_bullet"_hash, "data/audio/bulletshoot.ogg", mData.sprg);

    //array textures
    loadAllArrayTextures(mData.sprg);

    //shaders
    loadAllShaders(mData.sprg);

    registerEntityStates(mData);
}

#include <scenarios/scenarios.hpp>
#include <physics/moveagainstlandscape.hpp>
#include <spr/camera/camera.hpp>
#include <painter/painter.hpp>

void Game::startScenario()
{
    scenarios::testScenario(mData);
}

void Game::setup(const std::vector<std::string>& args)
{
    if(args.size() > 1 && args[1] == "p")
        mData.paused = true;

    spr::registerInputGroup(InputGroup::Debug, true, mData.inputData);
    spr::registerInputGroup(InputGroup::Global, true, mData.inputData);
    spr::registerInputGroup(InputGroup::Gameplay, true, mData.inputData);

    spr::addBinding(InputGroup::Debug, {}, spr::KeyCode::Comma, spr::DebugAction::ToggleTables, 0, &mData.showTables, mData.inputData);

    spr::addBinding(InputGroup::Debug, {}, spr::KeyCode::Period, spr::DebugAction::ToggleProfiler, 0, &mData.showProfiler, mData.inputData);
    spr::addBinding(InputGroup::Debug, {}, spr::KeyCode::Slash, spr::DebugAction::ToggleDebugMenu, 0, &mData.showDebugMenu, mData.inputData);
    spr::addBinding(InputGroup::Debug, {}, spr::KeyCode::M, "show_mouse_action"_hash, 0, &mData.showMouse, mData.inputData);

    spr::addBinding(InputGroup::Gameplay, {}, spr::KeyCode::A, PlayerAction::Left, 0, nullptr, mData.inputData);
    spr::addBinding(InputGroup::Gameplay, {}, spr::KeyCode::D, PlayerAction::Right, 0, nullptr, mData.inputData);
    spr::addBinding(InputGroup::Gameplay, {}, spr::KeyCode::W, PlayerAction::Jump, 0, nullptr, mData.inputData);
    spr::addBinding(InputGroup::Gameplay, {}, spr::KeyCode::S, PlayerAction::Down, 0, nullptr, mData.inputData);
    spr::addBinding(InputGroup::Gameplay, {}, spr::KeyCode::E, PlayerAction::Dig, 0, nullptr, mData.inputData);
    spr::addBinding(InputGroup::Gameplay, {}, spr::KeyCode::Q, PlayerAction::Throw, 0, nullptr, mData.inputData);

    spr::addBinding(InputGroup::Global, {}, spr::KeyCode::P, GlobalAction::Pause, 0, &mData.paused, mData.inputData);
    spr::addBinding(InputGroup::Global, {}, spr::KeyCode::O, GlobalAction::AdvanceFrame, 0, nullptr, mData.inputData);
}

void erode(GameData& data)
{
    bool predicateLeftPressed = data.inputData.ongoingMousePresses.count(spr::MouseButton::Left);
    bool predicateRightPressed = data.inputData.ongoingMousePresses.count(spr::MouseButton::Right);

    auto reduceToPaintOperation = [] (auto left, auto right)
    {
        using Result = std::optional<LithosType>;
        return left ? Result{LithosType::Soil} : right ? Result{LithosType::Air} : Result{};
    };

    auto operation = reduceToPaintOperation(predicateLeftPressed, predicateRightPressed);

    if(operation)
    {
        auto [viewportId, cameraId] = data.renderData.getObservingViewportAndCamera();
        WorldCoordinate mouseWorld = spr::screenToWorld(viewportId, cameraId, data.inputData.mousePosition, data.sprg);

        paintCircle(mouseWorld, 16.0f, [&](glm::ivec2 paintPos, int magnitude)
        {
            bool magZero = *operation == LithosType::Air;
            data.lithosData.types.setOrNoop(paintPos, *operation);
            data.lithosData.colorVariation.setOrNoop(paintPos, magZero ? int8_t{} : (std::max)(int8_t(magnitude), data.lithosData.colorVariation.atOr(paintPos, {})));
            data.renderData.invalidateUnit(paintPos);
        });
    }
}

void Game::loop()
{
    mFrameLogic.newFrame();
    
    //mouse capture if it is enabled
    mData.inputData.captureMouse = !mData.showMouse;

    while(mFrameLogic.timeRemains())
    {
        mFrameLogic.advanceDeltaTime();

        spr::sprEnsureCapacity(1024, mData.sprg);
        ensureCapacity(1024, mData.gamg);
        spr::sprEnsureCapacity(1024, mData.worldScene->spr);
        spr::sprEnsureCapacity(1024, mData.uiScene->spr);
        ensureCapacity(1024, mData.worldScene->game);
        ensureCapacity(1024, mData.uiScene->game);

#ifdef DEBUG_ON
        spr::SprTablesCapacity sprgCapacitiesBefore = spr::sprTablesCapacity(mData.sprg);
        spr::SprTablesCapacity worldScene->sprCapacitiesBefore = spr::sprTablesCapacity(mData.worldScene->spr);
        DataTablesCapacity capacitiesBefore = tablesCapacity(mData.game);
#endif

        mInputLogic.readInput();
        handleSystemInput();

        erode(mData);

        updateInterface(mData);

        bool advanceFrame = !mData.paused || mData.advancePaused;
        if(advanceFrame)
        {
            processPlayerIntelligence(mData);

            updateLocomotionForces(mData);
            updateEnvironmentalForces(*mData.worldScene);
            applyAndResetAcceleration(*mData.worldScene);
            auto collisions = moveAgainstLandscape(mData);
            handleClonkCollisions(mData, collisions);
            handleClonkWalking(mData);

            updateLandscapeAttach(mData);

            spr::updateWorldPositions(mData.worldScene->spr);
            spr::updateWorldPositions(mData.uiScene->spr);

            updateSpatialTree(mData);

            mCollisionLogic.update(&mData.spatialEntityStorage);


            //entity logic
            {
                ZoneScopedN("entity removal logic");
                auto entitiesToRemove = mEntityStatesLogic.update();

                for(dpx::TableId toRemove : entitiesToRemove)
                {
                    removeEntity(toRemove, *mData.worldScene, mData);
                }

                removeDeletedEntities(mData);
            }

            updateEntitySprites(mData);

            if(mData.advancePaused > 0)
                --mData.advancePaused;

            mInputLogic.clearStartedAndStopped();

            //debug
            {
                ZoneScopedN("debug renderer")
                updateDebugDrawables(*mData.worldScene);
            }
            spr::DRen::updateTtl(DebugCode::game);
        }

#ifdef DEBUG_ON
        spr::SprTablesCapacity sprCapacitiesAfter = spr::sprTablesCapacity(mData.sprg);
        spr::SprTablesCapacity sprCapacitiesAfter = spr::sprTablesCapacity(mData.worldScene->spr);
        DataTablesCapacity capacitiesAfter = tablesCapacity(mData.game);
    
        TH_ASSERT(sprCapacitiesBefore == sprCapacitiesAfter, "Spawning crossed capacity boundary in the middle of frame");
        TH_ASSERT(capacitiesBefore == capacitiesAfter, "Spawning crossed capacity boundary in the middle of frame");
#endif
    }

    //update camera position if needed
    updateCameraFollow(mData);

    spr::DRen::updateTtl(DebugCode::render);

    {
        ZoneScopedN("render frame start")
        mRenderLogic.frameStart(mData.c->world->voidColor);
    }

    {
        ZoneScopedN("show datatables")
        if(mData.showTables)
        {
            spr::showDataTables("Spring tables", mClickedEntity, mData.sprg, mData.worldScene->spr);
            showDataTables("Game tables", mClickedEntity, mData.gamg, mData.worldScene->game, mData.sprg, mData.worldScene->spr);
        }
    }

    if(mData.showDebugMenu)
    {
        ZoneScopedN("show debug menu")
        spr::showDebugMenu(mData.sprg);
    }

    {
        ZoneScopedN("rendering")
        mRenderLogic.renderFrame();
    }

    {
        ZoneScopedN("swap")
        mWindow.swapBuffers();
    }
    FrameMark; //tracy framemark

    mAudioLogic.update();
}

void Game::handleSystemInput()
{
    if(mData.inputData.systemInput.quit)
    {
        quit();
    }
    
    if(mData.inputData.systemInput.resized)
    {
        glm::ivec2 size = *mData.inputData.systemInput.resized;
        mData.screenSize = size;

        mRenderLogic.resizeWindow(mData.screenSize);
    }

    if(mData.inputData.startedActions.count(GlobalAction::AdvanceFrame))
    {
        ++mData.advancePaused;
    }

    if(mData.inputData.mouseWheel)
    {
        int delta = *mData.inputData.mouseWheel;
        mData.zoomFactor += delta * 4;

        mRenderLogic.resizeWindow(mData.screenSize);
    }
}
