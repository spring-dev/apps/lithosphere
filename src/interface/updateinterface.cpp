#include <gamedata.hpp>
#include <spr/gl/viewport.hpp>
#include <spr/camera/camera.hpp>
#include <spr/data/camera.hpp>

void updateInterface(GameData& data)
{
    if(!data.inputData.startedMousePresses.empty())
    {
        glm::ivec2 mouseScreenPos = data.inputData.mousePosition;

        dpx::TableId observingCam = data.renderData.worldCameras[data.renderData.observingWorldCamera];
        glm::vec2 mouseNdc = spr::gl::viewportTransformFromWindowToNdc(data.renderData.mainViewport, static_cast<glm::vec2>(mouseScreenPos), data.sprg);
        glm::vec2 mouseWorld = spr::swizzle("xy", spr::cameraUntransformFromNdc(observingCam, glm::vec3(mouseNdc, 0.0f), data.sprg));

        spr::Camera& cam = get(observingCam, *data.sprg.tCamera);
        cam.translation = {mouseWorld.x, mouseWorld.y, cam.translation.z};
    }
}
