#include "clonkbehavior.hpp"
#include <gamedata.hpp>
#include <dpx/join.hpp>
#include <dpx/disable.hpp>
#include <spr/data/physics.hpp>
#include <data/environmentalforces.hpp>
#include <data/landscapeattach.hpp>
#include <data/alltypes.hpp>
#include <data/landscapecollider.hpp>
#include <data/clonkbehavior.hpp>
#include <spr/sprite/animation.hpp>
#include <spr/resources/animation.hpp>

#include <painter/painter.hpp>

constexpr float cAttachTolerance = 10.0f;

std::optional<ClonkState> getClonkState(dpx::TableId id, struct GameData &data)
{
    if(findId(id, *data.worldScene->game.tClonkBehavior) == nullptr)
        return {};

    spr::Physics* physics        = findId(id, *data.worldScene->spr.tPhysics);
    LandscapeAttach* attach      = findId(id, *data.worldScene->game.tLandscapeAttach);
    MoveIntention* moveIntention = findId(id, *data.worldScene->game.tMoveIntention);

    if(!physics)
        return {};

    if(attach)
    {
        float xSpeed = std::fabs(physics->velocity.x);
        float xIntention = moveIntention ? std::fabs(moveIntention->direction.x * moveIntention->speedFactor) : 0.0f;
        float threshold = 0.5f;
        if(xSpeed > threshold && xIntention > threshold)
            return ClonkState::Walking;
        else
            return ClonkState::Standing;
    }
    else
        return ClonkState::Falling;
}

bool canClonkJump(std::optional<ClonkState> ClonkState)
{
    return ClonkState ? (*ClonkState == ClonkState::Standing || *ClonkState == ClonkState::Walking) : false;
}

bool isClonkOnGround(std::optional<ClonkState> ClonkState)
{
    return ClonkState ? (*ClonkState == ClonkState::Standing || *ClonkState == ClonkState::Walking) : false;
}

bool isClonkInAir(std::optional<ClonkState> ClonkState)
{
    return ClonkState ? !isClonkOnGround(ClonkState) : false;
}


void handleClonkCollisions(GameData& data, const std::vector<LandscapeCollision>& collisions)
{
    dpx::join([&](dpx::TableId id, const ClonkBehavior& clonkBehavior, spr::Physics& physics, LandscapeCollider& collider)
    {
        const LandscapeCollision* collision = nullptr;

        for(const LandscapeCollision& currentCollision : collisions)
        {
            if(id == currentCollision.colliderId)
            {
                collision = &currentCollision;
            }
        }

        auto ClonkState = getClonkState(id, data);

        if(collision && isClonkInAir(ClonkState))
        {
            data.events("[fall]{collide@" + data.events.s(collision->impactPoint) + "} vel:[" + data.events.s(physics.velocity) + "->");

            auto collisionVelocityReduce = [] (float velocity, auto type, auto zeroType) { return type == zeroType ? 0.0f : velocity;};

            physics.velocity.x = collisionVelocityReduce(physics.velocity.x, collision->collisionType, LandscapeCollision::Vertical);
            physics.velocity.y = collisionVelocityReduce(physics.velocity.y, collision->collisionType, LandscapeCollision::Horizontal);

            if(collision->collisionType == LandscapeCollision::Horizontal)
            {
                data.events(data.events.s(physics.velocity) + "] state->[stand]");

                collider.stopMovement = false;

                disable(id, *data.worldScene->game.tEnvironmentalForces);
                size_t attachVertexIndex = 2;
                insert(id, LandscapeAttach{AxisOrientation::Vertical, attachVertexIndex, cAttachTolerance, true}, *data.worldScene->game.tLandscapeAttach);

                data.events(" attaching vertex " + std::to_string(attachVertexIndex) + "\n");
            }
        }

    }, *data.worldScene->game.tClonkBehavior, *data.worldScene->spr.tPhysics, *data.worldScene->game.tLandscapeCollider);
}

void handleClonkWalking(struct GameData& data)
{
    dpx::join([&](dpx::TableId id, const ClonkBehavior& clonkBehavior, spr::Physics& physics)
    {
        auto ClonkState = getClonkState(id, data);

        if(!ClonkState)
            return dpx::LoopResult::Continue;

        bool detach = false;

        if(canClonkJump(ClonkState) && data.inputData.ongoingActions.count(PlayerAction::Jump))
        {
            physics.velocity.y -= 7.0f;
            detach = true;
        }

        if(isClonkOnGround(ClonkState))
        {
            if(physics.velocity.x < 0.0f)
                setOrientation(id, HOrientation::Left, data);
            else if(physics.velocity.x > 0.0f)
                setOrientation(id, HOrientation::Right, data);

            LandscapeAttach* attach = findId(id, *data.worldScene->game.tLandscapeAttach);

            detach |= !attach->didAttach;
        }

        if(detach)
        {
            erase(id, *data.worldScene->game.tLandscapeAttach);
            enable(id, *data.worldScene->game.tEnvironmentalForces);

            get(id, *data.worldScene->game.tLandscapeCollider).stopMovement = true;
        }

        if(*ClonkState == ClonkState::Walking)
            spr::switchAnimationInSet(id, "walk"_hash, spr::DefaultFallback::No, data.worldScene->spr, data.sprg);
        else if(*ClonkState == ClonkState::Standing)
            spr::switchAnimationInSet(id, "idle"_hash, spr::DefaultFallback::No, data.worldScene->spr, data.sprg);
        else if(*ClonkState == ClonkState::Falling)
            spr::switchAnimationInSet(id, "tumble"_hash, spr::DefaultFallback::No, data.worldScene->spr, data.sprg);

        DigIntention* digIntention = findId(id, *data.worldScene->game.tDigIntention);
        if(digIntention)
        {
            WorldCoordinate pos = spr::swizzle("xy", get(id, *data.worldScene->spr.tWorldPosition).coordinate);

            paintCircle(pos, 10.0f, [&](glm::ivec2 paintPos, int magnitude)
            {
                data.lithosData.types.setOrNoop(paintPos, LithosType::Air);
                data.lithosData.colorVariation.setOrNoop(paintPos, int8_t{});
                data.renderData.invalidateUnit(paintPos);
            });
        }

        return dpx::LoopResult::Continue;
    }, *data.worldScene->game.tClonkBehavior, *data.worldScene->spr.tPhysics);
}