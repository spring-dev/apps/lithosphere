#pragma once

#include <thero/meta_enum.hpp>

meta_enum_class(ClonkState, int, Falling , Standing, Walking);