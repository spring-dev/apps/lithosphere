#pragma once
#include <physics/moveagainstlandscape.hpp>
#include "clonkdefines.hpp"

std::optional<ClonkState> getClonkState(dpx::TableId id, struct GameData& data);
bool canClonkJump(std::optional<ClonkState> state);
bool isClonkOnGround(std::optional<ClonkState> state);
bool isClonkInAir(std::optional<ClonkState> state);

void handleClonkCollisions(struct GameData& data, const std::vector<LandscapeCollision>& collisions);
void handleClonkWalking(struct GameData& data);