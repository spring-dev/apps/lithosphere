#version 300 es

layout(location=0) in vec4 position; //will be compatible with vec2 and vec3 attributes. extended to vec4: (0, 0, 0, 1) for missing components
layout(location=1) in vec3 normal;
layout(location=2) in vec2 texCoord;
layout(location=3) in uint textureLayer;

uniform mat4 viewProjection;

out vec3 fragmentNormal;
out vec2 fragmentTexCoord;
flat out uint layer;

void main()
{
    gl_Position = viewProjection * position;
    fragmentNormal = normal;
    fragmentTexCoord = texCoord;
    layer = textureLayer;
}
