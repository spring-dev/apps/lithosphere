#version 300 es

precision mediump float;
precision mediump sampler2DArray;

in vec3 fragmentNormal;
in vec2 fragmentTexCoord;
flat in uint layer;

uniform sampler2DArray arrayTexture;
uniform vec3 sunDirection;

out vec4 outColor;

vec3 surfaceAmbient = vec3(0.7, 0.7, 0.7);
vec3 sunAmbient = vec3(0.7, 0.7, 0.7);
vec3 surfaceDiffuse = vec3(1.0, 1.0, 1.0);
vec3 sunDiffuse = vec3(1.0, 1.0, 1.0);

void main()
{
    vec3 ambientColor = surfaceAmbient * sunAmbient;
    float lightAngleFactor = max(0.0, dot(fragmentNormal, sunDirection));
    vec3 diffuseColor = surfaceDiffuse * sunDiffuse * lightAngleFactor;
    vec3 finalColor = ambientColor + diffuseColor;
    vec4 textureColor = texture(arrayTexture, vec3(fragmentTexCoord, layer));
    outColor = vec4(finalColor, 1.0) * textureColor;
}
