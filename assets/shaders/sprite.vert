#version 300 es

layout(location=0) in vec4 position; //will be compatible with vec2 and vec3 attributes. extended to vec4: (0, 0, 0, 1) for missing components
layout(location=1) in vec2 texCoord;
layout(location=2) in vec4 color;

uniform mat4 viewProjection;

out vec2 vTex;
out vec4 vColor;

void main()
{
    gl_Position = viewProjection * position;
    vTex = texCoord;
    vColor = color;
}
